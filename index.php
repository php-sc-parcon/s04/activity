<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity</title>
</head>
<body>

	<h1>Building</h1>

	<?php $condominium->setName('Caswynn Building') ?>
	<?php $condominium->setFloors(8) ?>
	<?php $condominium->setAddress('Timog Avenue, Quezon City, Philippines') ?>

	<p>The name of the building is <?php echo $condominium->getName(); ?></p>
	<p>The <?php echo $condominium->getName()?> has <?php echo $condominium->getFloors() ?> floors.</p>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>

	<?php $condominium->setName('Caswynn Complex') ?>
	<p>The name of the building has been changed to <?php echo $condominium->getName(); ?></p>





	<h1>Condominium</h1>

	<?php $condominium->setName('Enzo Condo') ?>
	<?php $condominium->setFloors(5) ?>
	<?php $condominium->setAddress('Buendia Avenue, Makati City, Philippines') ?>

	<p>The name of the building is <?php echo $condominium->getName(); ?></p>
	<p>The <?php echo $condominium->getName()?> has <?php echo $condominium->getFloors() ?> floors.</p>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>

	<?php $condominium->setName('Enzo Tower') ?>
	<p>The name of the building has been changed to <?php echo $condominium->getName(); ?></p>

	

</body>
</html>